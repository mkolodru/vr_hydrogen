import numpy
import glob
#import networkx as nx

f_lst=glob.glob('input_data/M_z0=*.csv')
for f in f_lst:
    M=numpy.loadtxt(f,delimiter=',')
    fout=open(f.replace('M_','M_sparse_'),'w')
    #    G=nx.Graph()
    basis_sz=M.shape[0]
    assert(basis_sz==M.shape[1])
    filled=numpy.zeros((basis_sz),dtype=int)
    fout.write('# Basis size = '+str(basis_sz)+'\n')
    for j in range(M.shape[0]):
        for k in range(j,M.shape[1]):
            if M[j,k] > 1e-8:
                fout.write(str(j)+','+str(k)+','+str(M[j,k])+'\n')
                # if j!=k:
                #     G.add_edge(j,k)
    # if not nx.is_connected(G):
    #     print('For f=',f,', graph is not connected')
    #     print('Nodes connected to index zero: ',nx.node_connected_component(G,0))
    # else:
    #     print('For f=',f,', graph is connected')
    # exit()
        
