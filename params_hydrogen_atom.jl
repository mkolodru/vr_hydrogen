# Parameter values, to be done from command line or separate file
dt=0.2
Gamma_decay=1.5 # Overall scale of decay
p_meas_each=0.1 # Probability to measure during given time step
sigma_meas=1.0 # Uncertainty in position for given measurement
n_max=10
dx=0.5 # Voxel grid step
x_max=10.25 # Voxel grid max - should not be multiple of dx
r_meas=5.0
t_max=10.0 # Evolve to max time for now
rand_seed=0
