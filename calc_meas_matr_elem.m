a0=1;
sigma=1;

psi[{n_, l_, m_}, {r_, theta_, phi_}] :=
  With[{rho = 2 r/(n a0)}, 
    Sqrt[(2/(n a0))^3 (n - l - 1)!/(2 n (n + l)!)] Exp[-rho/2] rho^l 
      LaguerreL[n - l - 1, 2 l + 1, rho] SphericalHarmonicY[l, m, theta, phi]]

(* Integrate[psi[{2,1,0},{r,theta,phi}]*Conjugate[psi[{2,1,0},{r,theta,phi}]] r^2 Sin[theta],{r,0,Infinity},{theta,0,Pi},{phi,0,2 Pi}] *)

n=2;
l=1;
m=1;
np=1;
lp=0;
mp=0;
dphi=Pi/100;

xmax=20.01
(*dx=0.05*)
dx=5.0

x0=0
y0=0

For[z0=0, z0 < xmax + dx/2, z0 += dx, 
val=Sum[NIntegrate[psi[{n,l,m},{r,theta,phi}]*Conjugate[psi[{np,lp,mp},{r,theta,phi}]]
Exp[-(r Sin[theta] Cos[phi] - x0)^2/(4 sigma^2)] Exp[-(r Sin[theta] Sin[phi] - y0)^2/(4 sigma^2)] Exp[-(r Cos[theta] - z0)^2/(4 sigma^2)] 
r^2 Sin[theta],{r,0,Infinity},{theta,0,Pi}],{phi,dphi/2,2 Pi,dphi}] dphi;
Print["Test"];
Print["z0=",InputForm[z0]];
Print["real=",InputForm[Re[val]]];
Print["imag=",InputForm[Im[val]]];];

(* RR[x_,y_,z_]=Sqrt[x^2 + y^2 + z^2];
TT[x_,y_,z_]=ArcCos[z/RR[x,y,z]];
EP[x_,y_,z_]=Piecewise[{{0,x==0&&y==0}},(x+I y)/Sqrt[x^2+y^2]]

NIntegrate[Exp[-RR[x,y,z]] RR[x,y,z]^3 Exp[-RR[x,y,z]/2] Sin[TT[x,y,z]]^2 EP[x,y,z]
Exp[-(x - x0)^2] Exp[-(y - y0)^2] Exp[-(z - z0)^2],{x,-20,20},{y,-20,20},{z,-20,20},WorkingPrecision->100] *)



