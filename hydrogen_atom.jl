# Calculate time dependence of hydrogen atom with radiative losses and occasional measurements by photons

using GSL
using LinearAlgebra
using HDF5
using Random
using SpecialFunctions

function get_psi_r(r,n,l,m)
    try
        rho=2.0*r/n
        rho=max(rho,1e-9)
        my_norm=sqrt(abs((2/n)^3/(2*n) * exp(lfactorial(n-l-1)-lfactorial(n+l))))
        return sf_laguerre_n(n-l-1,2*l+1,rho)*exp(-rho/2)*rho^l*my_norm
    catch err
        println("r=$r")
        println("n=$n")
        println("l=$l")
        println("m=$m")
        println("Threw off error: $err")
    end
end

function get_psi_th(cth,l,m)
    cc=(-1.0)^m
    my_norm=sqrt(abs((2*l+1)/(4*pi)*exp(lfactorial(l-m) - lfactorial(l+m))))
    if m < 0
        cc*=exp(lfactorial(l+m)-lfactorial(l-m))
    end
    return cc*sf_legendre_Plm(l,abs(m),cth)*my_norm
end

# function get_psi_r(r,n,l,m)
#     rho=2.0*r/n
#     rho=max(rho,1e-9)
#     my_norm=sqrt((2/n)^3 * factorial(n-l-1)/(2*n*factorial(n+l)))
#     return sf_laguerre_n(n-l-1,2*l+1,rho)*exp(-rho/2)*rho^l*my_norm
# end
#
# function get_psi_th(cth,n,l,m)
#     cc=(-1.0)^m
#     my_norm=sqrt((2*l+1)*factorial(l-m)/(4*pi*factorial(l+m)))
#     if m < 0
#         cc*=(factorial(l+m)/factorial(l-m))
#     end
#     return cc*sf_legendre_Plm(l,abs(m),cth)*my_norm
# end

function get_psi(r,cth,phi,n,l,m)
    return exp(im*m*phi)*get_psi_r(r,n,l,m)*get_psi_th(cth,l,m)
end

function run_main()
    include("params_hydrogen_atom.jl")

    if rand_seed >= 0
        Random.seed!(rand_seed)
    end

    grid=-x_max:dx:(x_max+dx/2) # pixel grid. need to figure out most useful way to make voxels

    x=[]
    y=[]
    z=[]
    for xval in grid
        for yval in grid
            for zval in grid
                append!(x,xval)
                append!(y,yval)
                append!(z,zval)
            end
        end
    end
    r=(x.^2 + y.^2 + z.^2).^0.5
    cth=z./r # = cos(theta)
    phi=atan.(y,x)

    inds=sortperm(r)
    x=x[inds]
    y=y[inds]
    z=z[inds]
    r=r[inds]
    cth=cth[inds]
    phi=phi[inds]

    grid_sz=0
    for j=1:length(r)
        if r[j] > x_max
            grid_sz=j+0
            break
        end
    end

    x=x[1:grid_sz]
    y=y[1:grid_sz]
    z=z[1:grid_sz]
    r=r[1:grid_sz]
    cth=cth[1:grid_sz]
    phi=phi[1:grid_sz]

    n=[]
    l=[]
    m=[]
    for nval in 1:n_max
        for lval in 0:(nval-1)
            for mval in -lval:lval
                append!(n,nval)
                append!(l,lval)
                append!(m,mval)
            end
        end
    end

    println("Initializing matrices")

    println("Determining energy differences")

    E=-1/n.^2
    basis_sz=length(n)
    rho=zeros(Complex,basis_sz,basis_sz) # Density matrix, start out in ground state
    rho[1,1]=1.0+0.0*im
    E_diff=zeros(basis_sz,basis_sz)
    for row in 1:basis_sz
        for col in 1:basis_sz
            E_diff[row,col]=E[row]-E[col]
        end
    end

    println("Reading in decay matrices")

    # Read in decay matrix elements from a file in form L ~ sqrt(Gamma)*|gamma><delta|
    @assert(n_max <= 10)
    f=h5open("data_decay_rates_nmax=10.h5","r")
    Gamma_lst=read(f,"rate_lst")
    gamma_ind_lst=read(f,"gam_lst")
    delta_ind_lst=read(f,"del_lst")
    close(f)

    # Artificially add decay from metastable 2,0,0 to 1,0,0 at rate that is 1/2 of max decay to 1,0,0
    ia=1 # 1,0,0
    ib=2 # 2,0,0
    @assert(n[ia]==1 && l[ia]==0 && m[ia]==0)
    @assert(n[ib]==2 && l[ib]==0 && m[ib]==0)
    max_rate=0
    for j in 1:length(gamma_ind_lst)
        if gamma_ind_lst[j]==ia
            max_rate=max(max_rate,Gamma_lst[j])
        end
    end
    push!(Gamma_lst,0.5*max_rate)
    push!(gamma_ind_lst,ia)
    push!(delta_ind_lst,ib)
    println("Adding artificial decay from 2,0,0 -> 1,0,0 at rate $(0.5*max_rate)")


    # # Old code - as all-to-all coupling of energy diffs times number
    # Gamma_lst=[]
    # gamma_ind_lst=[]
    # delta_ind_lst=[]
    # alpha=0.1 # Let decay rates be alpha*(E_m-E_n)
    # for j in 1:basis_sz
    #     for k in (j+1):basis_sz
    #         if E[k] > E[j]
    #             push!(Gamma_lst,alpha*(E[k]-E[j]))
    #             push!(gamma_ind_lst,j)
    #             push!(delta_ind_lst,k)
    #         end
    #     end
    # end

    decay_matrix_coh=zeros(basis_sz,basis_sz) # Decay of coherences <-> off-diag of rho
    decay_matrix_ampl=zeros(basis_sz,basis_sz) # Decay of ampl <-> diag of rho

    for j in 1:length(gamma_ind_lst)
        gi=gamma_ind_lst[j]
        di=delta_ind_lst[j]
        G=Gamma_lst[j]
        if gi > basis_sz || di > basis_sz
            continue
        end
        decay_matrix_ampl[gi,di] += G # d(p[gamma])/dt ~ Gamma*p[delta]
        decay_matrix_ampl[di,di] -= G # d(p[delta])/dt ~ -Gamma*p[delta]
        for alpha in 1:basis_sz
            if alpha != di
                decay_matrix_coh[di,alpha] -= G/2
                decay_matrix_coh[alpha,di] -= G/2
            end
        end
    end

    # Exponentiate the amplitude decay matrix
    exp_decay_matrix_ampl=exp(dt*decay_matrix_ampl*Gamma_decay)

    println("Constructing wfns at each site")

    # Wavefunctions at each site
    psi_grid=zeros(Complex,basis_sz,grid_sz)
    for j in 1:basis_sz
        if j%10==1
            println("At j=$j out of $basis_sz")
        end
        for k in 1:grid_sz
            psi_grid[j,k]=get_psi(r[k],cth[k],phi[k],n[j],l[j],m[j])
        end
    end
    for j in 1:grid_sz
        if j%10==1
            println("At j=$j out of $grid_sz")
        end
        psi_grid[:,j]=psi_grid[:,j]./sum(abs.(psi_grid[:,j].^2))^0.5
    end

    # Constructing measurement weights

    println("Constructing meas weights")

    # (Unnormalized) Gaussian weights for weak measurement
    weight_meas=zeros(grid_sz,grid_sz)
    for row in 1:grid_sz
        if row%10==1
            println("At row=$row out of $grid_sz")
        end
        for col in 1:grid_sz
            weight_meas[row,col]=exp(-((x[row]-x[col])^2 + (y[row] - y[col])^2 + (z[row] - z[col])^2)/(2*sigma_meas^2))
        end
    end

    println("Creating measurement operators from weights + wfns")

    # Measurement operators
    # Old code which is too slow - currently just do one meas op
    # M_meas=zeros(Complex,grid_sz,basis_sz,basis_sz)
    # for g in 1:grid_sz
    #     if g%10==1
    #         println("At g=$g out of $grid_sz")
    #     end
    #     # proj=psi_grid[:,g]*psi_grid[:,g]' # = |r><r| (summed over)
    #     for g0 in 1:grid_sz
    #         for j in 1:basis_sz
    #             for k in 1:basis_sz
    #                     M_meas[g0,j,k]=exp(-((x[g]-x[g0])^2+(y[g]-y[g0])^2+(z[g]-z[g0])^2)/(4*sigma_meas^2))*psi_grid[j,g]*conj(psi_grid[k,g])
    #             end
    #         end
    #     end
    # end

    M_meas=zeros(Complex,1,basis_sz,basis_sz)
    #g0=Int64(round(0.25*grid_sz))#rand(1:grid_sz)
    g0=0
    for g in 1:grid_sz
        if r[g]>=r_meas
            g0=g-0
            break
        end
    end
    for g in 1:grid_sz
        if g%10==1
            println("At g=$g out of $grid_sz")
        end
        # proj=psi_grid[:,g]*psi_grid[:,g]' # = |r><r| (summed over)
        for j in 1:basis_sz
            for k in 1:basis_sz
                M_meas[1,j,k]+=exp(-((x[g]-x[g0])^2+(y[g]-y[g0])^2+(z[g]-z[g0])^2)/(4*sigma_meas^2))*psi_grid[j,g]*conj(psi_grid[k,g])
            end
        end
    end

    t=0:dt:(t_max+dt/2)
    p=zeros(grid_sz,length(t)) # Probability density for each voxel
    p_basis=zeros(basis_sz,length(t))
    meas_ind=zeros(length(t)) # Measurement record of Gaussian centers. 0=no meas

    println("Evalaluating observable for initial state")

    # Evaluate p(r) for display
    for g in 1:grid_sz
        p[g,1]=real(psi_grid[:,g]'*(rho*psi_grid[:,g]))
    end
    p[:,1]=p[:,1]./sum(p[:,1])

    dinds=diagind(rho)
    p_basis[:,1]=real.(rho[dinds])

    println("Starting time evolution")

    for j in 2:length(t)
        if j % 5 == 0
            println("At step j=$j out of $(length(t))")
        end

        # First time evolve for dt
        rho=exp.((-im.*E_diff .+ Gamma_decay .* decay_matrix_coh).*dt) .* rho # Coherences and energy diffs are element-wise
        rho[dinds]=exp_decay_matrix_ampl*rho[dinds]

        # Then calculate p
        for g in 1:grid_sz
            p[g,j]=real(psi_grid[:,g]'*(rho*psi_grid[:,g]))
        end
        p[:,j]/=sum(p[:,j])

        # Temporary - measure at step 1 with chosen op then no more measurements
        if j==2
            rho=M_meas[1,:,:]*rho*M_meas[1,:,:]
            rho=rho./tr(rho) # Normalize the density matrix
        end

        # Temporary - output data just after first measurement, then kill it

        p_after=zeros(grid_sz)
        for g in 1:grid_sz
            p_after[g]=real(psi_grid[:,g]'*(rho*psi_grid[:,g]))
        end
        p_after/=sum(p_after)
        h5write("p_after.h5","p_after",p_after)
        return

        # Then do weak measurement with prob. p_meas_each
        # if rand() > p_meas_each
        #     continue
        # end
        # # Old code for random measurement - use later
        # # p_meas=weights_meas*p[:,j] # Prob of meas outcomes (unnorm'd)
        # # p_meas/=sum(p_meas)
        # # P_meas=cumsum(p_meas)
        # # r=rand()
        # # mi=binary_search(r,P_meas)
        # # meas_ind[j]=mi
        #
        # mi=1
        #
        # rho=M_meas[mi,:,:]*rho*M_meas[mi,:,:]
        # rho=rho./tr(rho) # Normalize the density matrix

        p_basis[:,j]=real.(rho[dinds])
    end

    # Then save data
    fout=h5open("data_hydrogen.h5","w")
    write(fout,"p",p)
    write(fout,"p_basis",p_basis)
    write(fout,"meas_ind",meas_ind)
    write(fout,"x",convert(Array{Float64,1},x))
    write(fout,"y",convert(Array{Float64,1},y))
    write(fout,"z",convert(Array{Float64,1},z))
    write(fout,"n",convert(Array{Int64,1},n))
    write(fout,"l",convert(Array{Int64,1},l))
    write(fout,"m",convert(Array{Int64,1},m))
    write(fout,"t",collect(t))
    close(fout)
end

run_main()
